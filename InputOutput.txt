Input Example 1:

List<Client> listClient = new ArrayList<>();
listClient.add(new Client(1, "abc", 6));
listClient.add(new Client(2, "ghi", 1));
listClient.add(new Client(1, "def", 4));
listClient.add(new Client(1, "zzz", 2));


Execution Output:

Key = 1, Value = Butler [ requestIds=[abc, zzz], allocatedHours=8]
Key = 2, Value = Butler [ requestIds=[def, ghi], allocatedHours=5]




Input Example 2:

List<Client> listClient = new ArrayList<>();
listClient.add(new Client(1, "abc", 6));
listClient.add(new Client(2, "ghi", 1));
listClient.add(new Client(1, "def", 4));
listClient.add(new Client(1, "zzz", 2));
listClient.add(new Client(2, "ghijk", 3));
listClient.add(new Client(3, "bbbb", 3));
listClient.add(new Client(3, "ccc", 3));

Execution Output:

Key = 1, Value = Butler [ requestIds=[abc, zzz], allocatedHours=8]
Key = 2, Value = Butler [ requestIds=[def, ghi, ghijk], allocatedHours=8]
Key = 3, Value = Butler [ requestIds=[bbbb, ccc], allocatedHours=6]