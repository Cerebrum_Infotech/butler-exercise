import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class ButlerDemo {

	public static void main(String[] ags) {

		List<Client> listClient = new ArrayList<>();
		listClient.add(new Client(1, "abc", 6));
		listClient.add(new Client(2, "ghi", 1));
		listClient.add(new Client(1, "def", 4));
		listClient.add(new Client(1, "zzz", 2));
		listClient.add(new Client(2, "ghijk", 3));
		listClient.add(new Client(3, "bbbb", 3));
		listClient.add(new Client(3, "ccc", 3));

		listClient = listClient.stream().sorted(Comparator.comparingInt(Client::getId)).collect(Collectors.toList());

		Map<Integer, Butler> requiredButlerList = new HashMap<>();
		int butlerIds = 1;
		Map<Integer, List<Integer>> butlerIdentityList = new HashMap<>();
		for (int i = 0; i < listClient.size(); i++) {

			Client clientData = listClient.get(i);
			if (!requiredButlerList.isEmpty()) {
				// Get last Butler
				Butler butler = null;
				List<Integer> list = butlerIdentityList.get(clientData.getHours());
				if (list != null && !list.isEmpty()) {
					butler = requiredButlerList.get(list.get(0));
					list.remove(0);
				} else {
					butler = requiredButlerList.get(butlerIds - 1);
				}

				// Check if hours for bulter are completed
				if (butler != null && butler.getAllocatedHours() >= 8) {
					butler = new Butler();
					butler.setAllocatedHours(clientData.getHours());
					List<String> requestIds = new ArrayList<>();
					requestIds.add(clientData.getRequest());
					butler.setRequestIds(requestIds);
					requiredButlerList.put(butlerIds, butler);

					++butlerIds;
					list = new ArrayList<>();
					list.add(butlerIds - 1);
					butlerIdentityList.put((8 - clientData.getHours()), list);
				} else {
					int asignableHours = butler.getAllocatedHours() + clientData.getHours();
					// check if compbined hours are less than 9
					if (asignableHours < 9) {
						butler.getRequestIds().add(clientData.getRequest());
						butler.setAllocatedHours(asignableHours);
					} else {

						// setting map for future use
						list = butlerIdentityList.get((8 - butler.getAllocatedHours()));
						if (list != null && !list.isEmpty()) {
							list.add(butlerIds - 1);
						} else {
							list = new ArrayList<>();
							list.add(butlerIds - 1);
							butlerIdentityList.put((8 - butler.getAllocatedHours()), list);
						}

						// Getting eligible butler for particular hours
						List<Integer> canUseButterList = butlerIdentityList.get(clientData.getHours());
						if (canUseButterList != null && !canUseButterList.isEmpty()) {
							butler = requiredButlerList.get(canUseButterList.get(0));
							butler.setAllocatedHours(butler.getAllocatedHours() + clientData.getHours());
							canUseButterList.remove(0);
						} else {
							butler = new Butler();
							butler.setAllocatedHours(clientData.getHours());
							List<String> requestIds = new ArrayList<>();
							requestIds.add(clientData.getRequest());
							butler.setRequestIds(requestIds);
							requiredButlerList.put(butlerIds, butler);
							butlerIds++;
						}

					}

				}
			} else {
				Butler butler = new Butler();
				butler.setAllocatedHours(clientData.getHours());
				List<String> requestIds = new ArrayList<>();
				requestIds.add(clientData.getRequest());
				butler.setRequestIds(requestIds);
				requiredButlerList.put(butlerIds, butler);
				butlerIds++;
			}

		}

		for (Entry<Integer, Butler> entry : requiredButlerList.entrySet())
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

	}

	public static class Client {

		int id;
		String request;
		int hours;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getRequest() {
			return request;
		}

		public void setRequest(String request) {
			this.request = request;
		}

		public int getHours() {
			return hours;
		}

		public Client(int id, String request, int hours) {
			super();
			this.id = id;
			this.request = request;
			this.hours = hours;
		}

		public void setHours(int hours) {
			this.hours = hours;
		}

	}

	public static class Butler {

		int id;
		List<String> requestIds;
		int allocatedHours;

		public List<String> getRequestIds() {
			return requestIds;
		}

		public void setRequestIds(List<String> requestIds) {
			this.requestIds = requestIds;
		}

		public int getAllocatedHours() {
			return allocatedHours;
		}

		public void setAllocatedHours(int allocatedHours) {
			this.allocatedHours = allocatedHours;
		}

		@Override
		public String toString() {
			return "Butler [ requestIds=" + requestIds + ", allocatedHours=" + allocatedHours + "]";
		}

	}

}
